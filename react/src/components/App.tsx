import React, { Component } from "react";
import { Col, Container, Row } from "react-bootstrap";
import Page from "./Page/Page";
import Sidebar from "./Sidebar/Sidebar";
import Skill from "./Sidebar/Skill";
import Counter from "./Sidebar/Counter";

export default class extends Component {
  render() {
    return (
      <Container fluid>
        <Counter />
        <Row>
          <Col xs sm lg={3}>
            <Sidebar />
            <Skill />
          </Col>
          <Col xs sm lg={9}>
            <Row>
              <Col xs={1} sm lg />
              <Col xs={12} sm={12} lg={12}>
                <Page />
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    );
  }
}
