import React, { Component } from "react";
import { Col, Container, Row } from "react-bootstrap";

export default class extends Component {
  render() {
    return (
      <Container fluid>
        <div className="employment">
          <Col xs sm lg={12} className="page-block">
            <hr className="page-separator" />
            <h2>Employment</h2>
            <hr className="page-separator" />
          </Col>
          <Col xs sm lg={12} className="page-block">
            <h3>
              <a href="https://plek.co/en" target="_blank">
                Plek
              </a>
            </h3>
            <h4>Backend developer</h4>
            <h4>Jan 2017 – Jan 2020</h4>
            <h4>Amsterdam - Netherlands</h4>
            <p className="page-block-text">
              Plek is an intranet, communication and workplace tool for
              companies. I learned the importance of code optimization and
              refactorization to improve quality, readability or performance, to
              handle memory leaks or reduce memory use, touching all the parts
              of the software development circle (from the design until the
              integration and deployment of the product), working with 3rd party
              services and different technologies like NodeJs, AngularJs, React,
              Docker, MongoDB, ElasticSearch or Redis.
            </p>

            <Row>
              <Col xs sm lg={12} className="page-block">
                <p className="page-block-text">
                  Plek also has a
                  <a href="https://plek.co/en/app-desktop" target="_blank">
                    {" "}
                    desktop{" "}
                  </a>
                  version of the platform, which is base on Electron to create
                  native applications for Windows, Mac OSx or Linux users. I was
                  the maintainer and also, I developed a system to release new
                  updates automatically when they're published.
                </p>
              </Col>
            </Row>

            <Row>
              <Col xs sm lg={12} className="page-block">
                <p className="page-block-text">
                  <a href="https://iwa-connect.org" target="_blank">
                    IWA{" "}
                  </a>
                  is a global social platform, with subscription and membership
                  features. On this project, I worked with external services
                  like GlobalCollect, the payment system of the platform, and
                  Turpin, used to send out orders for the magazines that the
                  users buy. The biggest challenge was to improve and support
                  the payment system, managing the exchange of currencies and
                  taxes base on each country law. New features implemented were
                  a discount system or the creation of new kinds of memberships.
                </p>
              </Col>
            </Row>
          </Col>

          <Col xs sm lg={12} className="page-block">
            <h3>
              <a href="https://ilumy.com/" target="_blank">
                Ilumy
              </a>
            </h3>
            <h4>Backend developer</h4>
            <h4>Jan 2017 – Jan 2020</h4>
            <h4>Amsterdam - Netherlands</h4>
            <p className="page-block-text">
              My role was the setup, configuration and maintenance (core and
              updates of dependencies, web analytics, general support,
              backups...) of the website. Focused on drupal websites and
              Typescript projects.
            </p>
            <Row>
              <Col xs sm lg={12} className="page-block">
                <p className="page-block-text">
                  <a
                    href="https://www.bigmove.nu/locatie/bigmove-amsterdam-west"
                    target="_blank"
                  >
                    BigMove App{" "}
                  </a>
                  required to develop a server to communicate the website and
                  mobile app of the client. Implemented some features like a
                  real-time chat (one-to-one and multi-room, over WebSocket), a
                  chatbot or a cronjob system to trigger actions as sending out
                  emails, push notifications web alerts or chat messages.
                </p>
              </Col>
            </Row>
            <Row>
              <Col xs sm lg={12} className="page-block">
                <p className="page-block-text">
                  For
                  <a href="https://30dagenopzee.nl/" target="_blank">
                    {" "}
                    The Indische Herinneringscentrum{" "}
                  </a>
                  , we made a backend project from scratch with NodeJs and
                  Typescript. The challenge was to code the logic to fetch part
                  of the content and routes displayed on the globe from a third
                  party. Also, a CMS system was developed to import the rest of
                  the data.
                </p>
              </Col>
            </Row>
            <Row>
              <Col xs sm lg={12} className="page-block">
                <p className="page-block-text">
                  The{" "}
                  <a href="https://mensenrechten.nl/nl" target="_blank">
                    {" "}
                    College voor de Rechten van de Mens{" "}
                  </a>
                  project consists on a drupal website and a custom CMS to
                  publish articles on
                  <a
                    href="https://mensenrechten.nl/nl/publicaties"
                    target="_blank"
                  >
                    {" "}
                    the website.{" "}
                  </a>
                  My main task was the setup, configuration and maintenance
                  (core and dependencies updates, web analytics, general
                  support, backups...) of the website. Developed custom drupal
                  modules to fit requirements.
                </p>
              </Col>
            </Row>
            <Row>
              <Col xs sm lg={12} className="page-block">
                <p className="page-block-text">
                  The
                  <a href="https://plek.co/nl" target="_blank">
                    {" "}
                    Plek.co{" "}
                  </a>
                  website is another drupal project. I was in charge of the
                  maintenance of the server, deploying updates, developing
                  custom modules or helping to integrate the website with
                  SharpSpring
                </p>
              </Col>
            </Row>
          </Col>

          <Col xs sm lg={12} className="page-block">
            <h3>
              <a href="https://www.sugerendo.com/en/" target="_blank">
                Sugerendo Sistemas
              </a>
            </h3>
            <h4>
              Analyst programmer (
              <a
                href={"assets/files/Reference-letter-César.pdf"}
                target="_blank"
              >
                Reference letter
              </a>
              )
            </h4>
            <h4>Feb 2016 – Jul 2016</h4>
            <h4>Madrid - Spain</h4>
            <Row>
              <Col xs sm lg={12} className="page-block">
                <p className="page-block-text">
                  Sugerendo Sistemas is a start-up located in Madrid where
                  between 6 and 10 people work. Sugerendo is engaged in
                  consulting and implementation of e-commerce for businesses
                  with free software platforms like Magento.
                </p>
              </Col>
            </Row>
            <Row>
              <Col xs sm lg={12} className="page-block">
                <p className="page-block-text">
                  The main function was to give support to projects with
                  e-commerce that required a certain technical solution. Mainly
                  my function consisted in the development of new modules and
                  features to add to the online commerce of each client, in the
                  part of frontend as well as backend.
                </p>
              </Col>
            </Row>
            <Row>
              <Col xs sm lg={12} className="page-block">
                <p className="page-block-text">
                  The development of new modules is done using the web languages
                  PHTML, HTML, PHP and JavaScript. Another part of my job was
                  the maintenance and management of online stores, from the
                  server configuration to the optimum configuration of the
                  administrator control panel.
                </p>
              </Col>
            </Row>
          </Col>
        </div>

        <div className="education">
          <Col xs sm lg={12} className="page-block">
            <hr className="page-separator" />

            <h2>Education</h2>
            <hr className="page-separator" />
          </Col>
          <Col xs sm lg={12} className="page-block">
            <Row>
              <Col xs sm lg={4}>
                <h5>Computer Engineer Degree</h5>
                <h5>UPSAM</h5>
                <h5>Sep 2012 – Jun 2016</h5>
                <h5>Madrid - Spain</h5>
              </Col>
              <Col xs sm lg={5}>
                <h5>Aeronautical and Airport Management Certificate</h5>
                <h5>Universidad San Pablo CEU</h5>
                <h5>Sep 2010 - Jun 2012</h5>
                <h5>Madrid - Spain</h5>
              </Col>
              <Col xs sm lg={3}>
                <h5>High School</h5>
                <h5>IES Astorga</h5>
                <h5>Sep 2004 - Jul 2010</h5>
                <h5>Astorga - Spain</h5>
              </Col>
            </Row>
          </Col>
        </div>
      </Container>
    );
  }
}
