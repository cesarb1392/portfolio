import React, { Component } from "react";

export default class extends Component {
  render() {
    return (
      <div className="sidebar-contact">
        <ul>
          <li>
            <img
              src={"assets/svg/phone.svg"}
              alt="mail"
              className="contact-icon"
            />
            <a href="tel:+31684983437" target="_blank">
              +31 684983437
            </a>
          </li>
          <li>
            <img
              src={"assets/svg/mail.svg"}
              alt="mail"
              className="contact-icon"
            />
            <a href={"mailto:" + "conctact@cesarb.dev"} target="_blank">
              contact@cesarb.dev
            </a>
          </li>
          <li>
            <img
              src={"assets/svg/cv.svg"}
              alt="mail"
              className="contact-icon"
            />
            <a href={"assets/files/CesarRB-CV.pdf"} target="_blank">
              curriculum vitae
            </a>
          </li>
          <li>
            <img
              src={"assets/svg/linkedin.svg"}
              alt="mail"
              className="contact-icon"
            />
            <a
              href={"https://www.linkedin.com/in/cesar-represa-bautista/"}
              target="_blank"
            >
              LinkedIn
            </a>
          </li>
          <li>
            <img
              src={"assets/svg/website.svg"}
              alt="mail"
              className="contact-icon"
            />
            <a href={"https://portfolio.cesarb.dev"} target="_blank">
              portfolio.cesarb.dev
            </a>
          </li>
          <li>
            <img
              src={"assets/svg/gitlab.svg"}
              alt="mail"
              className="contact-icon"
            />
            <a href={"https://gitlab.com/cesarb1392"} target="_blank">
              @cesarb1392
            </a>
          </li>
        </ul>
      </div>
    );
  }
}
