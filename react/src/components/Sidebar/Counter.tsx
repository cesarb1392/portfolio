import React, { Component } from "react";
import {
  isMobile,
  isMobileOnly,
  isBrowser,
  isSmartTV,
  isWearable,
  isConsole,
  isAndroid,
  isWinPhone,
  isIOS,
  isChrome,
  isFirefox,
  isSafari,
  isOpera,
  isIE,
  isEdge,
  isYandex,
  isChromium,
  isMobileSafari,
  osVersion,
  osName,
  fullBrowserVersion,
  browserVersion,
  browserName,
  mobileVendor,
  mobileModel,
  engineName,
  engineVersion,
  getUA,
  deviceType,
  isIOS13,
  isIPhone13,
  isIPad13,
  isIPod13,
  isElectron,
  deviceDetect,
} from "react-device-detect";

export default class extends Component<
  {},
  { counter: number; csrf: string; data: object }
> {
  constructor(props: number) {
    super(props);
    this.state = {
      counter: 0,
      data: {},
      csrf: "",
    };
  }

  private getClientData() {
    return {
      isMobile,
      isMobileOnly,
      isBrowser,
      isSmartTV,
      isWearable,
      isConsole,
      isAndroid,
      isWinPhone,
      isIOS,
      isChrome,
      isFirefox,
      isSafari,
      isOpera,
      isIE,
      isEdge,
      isYandex,
      isChromium,
      isMobileSafari,
      osVersion,
      osName,
      fullBrowserVersion,
      browserVersion,
      browserName,
      mobileVendor,
      mobileModel,
      engineName,
      engineVersion,
      getUA,
      deviceType,
      isIOS13,
      isIPhone13,
      isIPad13,
      isIPod13,
      isElectron,
      deviceDetect,
    };
  }

  private async send() {
    try {
      const rawResponse = await fetch("/api", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify(this.state.data),
      });
      const { visitCounter } = await rawResponse.json();
      this.setState({ counter: parseInt(visitCounter) });
    } catch (e) {
      console.log(`BANANA error ${e}`);
    }
  }

  componentDidMount() {
    const data = this.getClientData();
    this.setState({ data }, async () => {
      this.send();
    });
  }

  render() {
    return <div></div>;
  }
}
