import React, { Component } from "react";

export default class extends Component {
  render() {
    return (
      <div className="sidebar-header">
        <img src={"assets/logo.png"} className="sidebar-logo" alt="logo" />

        <p className="sidebar-name">
          <b>César Represa </b>
          <b> Bautista</b>
        </p>
        <hr className="sidebar-separator" />
        <p className="sidebar-name">
          <b>Software </b> <b>Developer</b>
        </p>
        <hr className="sidebar-separator" />
      </div>
    );
  }
}
