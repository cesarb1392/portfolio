import React, { Component } from "react";
import { Col, Container, Row } from "react-bootstrap";
import Header from "./Header";
import Contact from "./Contact";

export default class extends Component {
  render() {
    return (
      <Container fluid className="sidebar-info">
        <Row className="sidebar-info-block">
          <Col xs sm lg={12}>
            <Header />
          </Col>
        </Row>
        <Row className="sidebar-info-block">
          <Col xs={12} sm={12} lg={12}>
            <Contact />
          </Col>
        </Row>
      </Container>
    );
  }
}
