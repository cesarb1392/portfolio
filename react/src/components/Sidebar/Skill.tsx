import React, { Component } from "react";
import { Col, Container, Row } from "react-bootstrap";

export default class extends Component {
  render() {
    return (
      <Container fluid className="sidebar-skill">
        <Row className="sidebar-skill-block">
          <Col xs sm lg={12}>
            <h2>Languages</h2>
            <hr className="sidebar-separator-skill" />
            <table className="two-column-table">
              <tbody>
                <tr>
                  <td>Javascript</td>
                  <td>Java</td>
                </tr>
                <tr>
                  <td>C/C++</td>
                  <td>PHP</td>
                </tr>
              </tbody>
            </table>
          </Col>
        </Row>

        <Row className="sidebar-skill-block">
          <Col xs sm lg={12}>
            <h2>Databases</h2>
            <hr className="sidebar-separator-skill" />
            <table className="two-column-table">
              <tbody>
                <tr>
                  <td>SQL/MySQL</td>
                  <td>PL/SQL</td>
                </tr>
                <tr>
                  <td>MongoDB</td>
                  <td>MariaDB</td>
                </tr>
              </tbody>
            </table>
          </Col>
        </Row>

        <Row className="sidebar-skill-block">
          <Col xs sm lg={12}>
            <h2>Technologies</h2>
            <hr className="sidebar-separator-skill" />
            <table className="two-column-table">
              <tbody>
                <tr>
                  <td>ElasticSearch</td>
                  <td>Redis</td>
                </tr>
                <tr>
                  <td>Docker</td>
                  <td>Electron</td>
                </tr>
                <tr>
                  <td>TypeScript</td>
                  <td>NodeJs</td>
                </tr>
                <tr>
                  <td>Drupal</td>
                  <td>Magento</td>
                </tr>
                <tr>
                  <td>Nginx</td>
                  <td>Apache</td>
                </tr>
              </tbody>
            </table>
          </Col>
        </Row>

        <Row className="sidebar-skill-block">
          <Col xs sm lg={12}>
            <h2>Hard Skills</h2>
            <hr className="sidebar-separator-skill" />
            <table className="two-column-table">
              <tbody>
                <tr>
                  <td>Algorithms</td>
                  <td>Data Structures</td>
                </tr>
                <tr>
                  <td>Network Config</td>
                  <td>System Admin</td>
                </tr>
                <tr>
                  <td>CI/CD</td>
                  <td>Git</td>
                </tr>
              </tbody>
            </table>
          </Col>
        </Row>
        <Row className="sidebar-skill-block">
          <Col xs sm lg={12}>
            <h2>Soft skills</h2>
            <hr className="sidebar-separator-skill" />
            <table className="two-column-table">
              <tbody>
                <tr>
                  <td>Collaboration</td>
                  <td>Adaptability</td>
                </tr>
                <tr>
                  <td>Problem Solving</td>
                  <td>Empathy</td>
                </tr>
              </tbody>
            </table>
          </Col>
        </Row>
      </Container>
    );
  }
}
