const env = process.env.NODE_ENV || "development";
const { ENV_CONFIG, ENV_RULES } =
  env === "development"
    ? require("./webpack.dev.config")
    : require("./webpack.prod.config");

const config = {
  mode: env,
  entry: "./src/index.tsx",
  module: {
    rules: [
      {
        test: /\.ts[x]?$/,
        exclude: /node_modules/,
        loader: ["babel-loader", "ts-loader"],
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
        },
      },
      {
        test: /\.(css)$/,
        exclude: /node_modules/,
        loader: ["style-loader", "css-loader"],
      },
      ...ENV_RULES,
    ],
  },
  resolve: {
    extensions: [".tsx", ".ts", ".js"],
  },
  performance: {
    hints: env !== "development",
  },
  optimization: {
    splitChunks: {
      chunks: "all",
    },
  },
  ...ENV_CONFIG,
};

module.exports = config;
