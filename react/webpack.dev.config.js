const path = require('path');
const webpack = require('webpack');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  ENV_CONFIG: {
    output: {
      filename: 'app.js',
      path: path.resolve(__dirname, 'build'),
    },
    plugins: [
      new CleanWebpackPlugin(),
      new webpack.HotModuleReplacementPlugin(),
      new HtmlWebPackPlugin({
        template: "./src/index.html",
        filename: "index.html"
      }),
      new CopyWebpackPlugin([{from: path.join('./src/assets'), to: 'assets'}]),
    ],
    devServer: {
      historyApiFallback: true,
      hot: true,
      inline: true,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': '*',
        'Access-Control-Allow-Methods': '*'
      },
      https: false,
      watchOptions: {
        aggregateTimeout: 300,
        ignored: /node_modules/,
        poll: false,
      },
      stats: 'minimal',
      port: 3003,
      host: 'localhost',
      disableHostCheck: false,
      contentBase: path.resolve(__dirname, 'public'),
      compress: true,
      overlay: {
        warnings: true,
        errors: true,
      },
      proxy: {
        '/api': {
          target: 'http://localhost:3000',
          changeOrigin: true,
          secure: false,
        },
        // '/csrf': {
        //   target: 'http://localhost:3000',
        //   changeOrigin: true,
        //   secure: false,
        // }
      }
    },
  },
  ENV_RULES: [
    {
      test: /\.(pdf|gif|png|jpe?g|svg|ico)$/,
      loader: 'file-loader',
      exclude: /node_modules/,
      options: {
        name: '[path][name].[ext]',
        outputPath: 'assets'
      },
    }
  ],

};

