const path = require("path");
const webpack = require("webpack");
const HtmlWebPackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = {
  ENV_CONFIG: {
    output: {
      filename: "app.js",
      path: path.resolve(__dirname, "build"),
    },
    plugins: [
      new CleanWebpackPlugin(),
      new webpack.HotModuleReplacementPlugin(),
      new HtmlWebPackPlugin({
        template: "./src/index.html",
        filename: "index.html",
      }),
      new CopyWebpackPlugin([
        { from: path.join("./src/assets"), to: "assets" },
      ]),
    ],
  },
  ENV_RULES: [
    {
      test: /\.(pdf|gif|png|jpe?g|svg|ico)$/,
      loader: "file-loader",
      exclude: /node_modules/,
      options: {
        name: "[path][name].[ext]",
        outputPath: "assets",
      },
    },
  ],
};
