import Server from "./config/server";

try {
  Server.bootstrap();
} catch (error) {
  throw new Error(`BANANA ERROR: ${error}\n\n`);
}
