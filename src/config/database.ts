import * as MongoDB from "mongodb";
import Counter from "../model/counter";

class Database {
  private db: MongoDB.Db;

  async connect(
    options?: { useUnifiedTopology: boolean; useNewUrlParser: boolean },
    secureConnectListener?: () => void
  ) {
    const client = await MongoDB.MongoClient.connect(
      `mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}`,
      options
    );
    this.db = client.db(process.env.DB_DATABASE);
  }

  async initModels() {
    await this.db.createCollection(Counter.MODEL_NAME);
  }

  async addOne(query: object, model: string) {
    return this.db.collection(`${model}`).insertOne(query);
  }

  async count(query: object, model: string) {
    return this.db.collection(`${model}`).countDocuments(query);
  }
}

module.exports = new Database();
