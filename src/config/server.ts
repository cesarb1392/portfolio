import * as BodyParser from "body-parser";
import * as Express from "express";
import * as Morgan from "morgan";
import * as Bunyan from "bunyan";
import * as cookieParser from "cookie-parser";
import * as Helmet from "helmet";
import * as Cors from "cors";
import * as path from "path";
import * as CSRF from "csurf";

const db = require("./database");

import VisitCounter from "../controller/counter";
import ErrorHandler, {
  ERROR_MSG,
  ERROR_NAME,
} from "../controller/errorHandler";

export default class Server {
  public app: Express.Application;
  private logger: Bunyan;

  public static bootstrap(): Server {
    return new Server();
  }

  constructor() {
    this.app = Express();
    this.initMiddleware();
    this.initRoutes();
    this.initDatabase()
      .then(() => console.log("MongoDb connected."))
      .catch((error) => console.log(`DB NOT CONNECTED ${error}`));
  }

  private initMiddleware() {
    require("dotenv").config();

    this.app.use(Express.static(path.join(__dirname, "../..", "react/build")));

    this.app.listen(parseInt(process.env.PORT));
    this.app.use(BodyParser.json());
    this.app.use(Helmet());
    this.app.use(cookieParser());
    this.setCors();
    this.setCSRF();
    this.initLogger();
  }

  private async initDatabase() {
    try {
      await db.connect({ useUnifiedTopology: true, useNewUrlParser: true });
      await db.initModels();
    } catch (e) {
      new Error(ERROR_NAME.OFFLINE_DB);
    }
  }

  private initRoutes() {
    const counter = new VisitCounter();
    this.app.get("/", counter.newVisit);
    this.app.post("/api", counter.newVisit);
    this.app.use(ErrorHandler.handler);
  }

  private initLogger() {
    this.logger = Bunyan.createLogger({
      name: process.env.APP_NAME,
      stream: process.stdout,
    });
    const morganOptions: Morgan.Options = {
      stream: {
        write: (message) => {
          this.logger.info(message);
        },
      },
    };
    this.app.use(Morgan("combined", morganOptions));
  }

  private setCors() {
    const whiteList = [process.env.ORIGIN1, process.env.ORIGIN2];
    this.app.use(
      Cors({
        origin: (req, callback) => {
          if (whiteList.includes(req)) {
            callback(null, true);
          } else {
            callback(new Error(ERROR_NAME.ORIGIN_NOT_ALLOWED), false);
          }
        },
        methods: ["GET", "POST"],
        optionsSuccessStatus: 200,
      })
    );
  }

  private setCSRF() {
    this.app.use(new CSRF({ cookie: true }));
    this.app.use((err, req, res, next) => {
      res.locals.csrfToken = req.csrfToken();
      next();
    });
    this.app.use((err, req, res, next) => {
      const error =
        err.code === ERROR_NAME.BAD_CSRF_TOKEN
          ? {
              name: ERROR_NAME.BAD_CSRF_TOKEN,
              message: ERROR_MSG.BAD_CSRF_TOKEN,
            }
          : { name: err.name, message: err.message };
      return ErrorHandler.handler(error, req, res, next);
    });
  }
}
