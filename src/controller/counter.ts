import * as Express from "express";
import Counter from "../model/counter";
import ErrorHandler, { ERROR_MSG, ERROR_NAME } from "./errorHandler";

const db = require("../config/database");

export default class VisitCounter {
  public async newVisit(req: Express.Request, res: Express.Response) {
    try {
      await db.addOne(
        { headers: req.headers, client: req.body, createdAt: new Date() },
        Counter.MODEL_NAME
      );
      return res.json({ counted: true });
    } catch (e) {
      ErrorHandler.handler(
        { message: ERROR_MSG.OFFLINE_DB, name: ERROR_NAME.OFFLINE_DB },
        req,
        res
      );
    }
  }

  private getVisitCount() {
    return db.count({}, Counter.MODEL_NAME);
  }
}
