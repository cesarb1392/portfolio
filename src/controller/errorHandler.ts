import * as Express from "express";

export enum ERROR_NAME {
  ORIGIN_NOT_ALLOWED = "ORIGIN_NOT_ALLOWED",
  BAD_CSRF_TOKEN = "EBADCSRFTOKEN",
  OFFLINE_DB = "OFFLINE_DB",
}

export enum ERROR_MSG {
  BAD_REQUEST = "Bad Request",
  ORIGIN_NOT_ALLOWED = "Origin not allowed",
  BAD_CSRF_TOKEN = "Invalid CSRF Token",
  OFFLINE_DB = "Can not connect to 3 party services",
}

export default class ErrorHandler {
  public static handler(
    err: Error,
    req: Express.Request,
    res: Express.Response,
    next?: Express.NextFunction
  ) {
    const error = {
      message: ERROR_MSG[err.message] || err.message || err.name,
    };
    let code = 400;
    switch (err.message) {
      case ERROR_NAME.ORIGIN_NOT_ALLOWED:
        code = 403;
        break;
      case ERROR_NAME.BAD_CSRF_TOKEN:
        code = 403;
        break;
      case ERROR_NAME.OFFLINE_DB:
        code = 503;
        break;
      default:
        break;
    }
    res.status(code).json(error);
    throw new Error(error.message);
  }
}
